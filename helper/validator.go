package helper

import (
	"fmt"
	"second-hand/util"
	"strings"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

func newValidator() (*validator.Validate, ut.Translator ){
	validate := validator.New()
	english := en.New()
	uni := ut.New(english, english)
	trans, ok := uni.GetTranslator("en")
	if !ok {
		panic("translator not found")
	}
	
	return validate,trans
}

func Validate(input interface{}) (map[string]string, bool){
    validate,trans := newValidator()
	translationError := en_translations.RegisterDefaultTranslations(validate, trans)
	util.PanicIfError(translationError)

    result := map[string]string{}

	err := validate.Struct(input)
	errs := translateError(err, trans)

    if len(errs) == 0 {
        return result,true
    }

	for _,item := range errs {
        s := strings.Split(item.Error(), " ")[0]
        result[s] = item.Error()
	}

    return result,false
}

func translateError(err error, trans ut.Translator) (errs []error) {
    if err == nil {
      return nil
    }
    validatorErrs := err.(validator.ValidationErrors)
    for _, e := range validatorErrs {
      translatedErr := fmt.Errorf(e.Translate(trans))
      errs = append(errs, translatedErr)
    }
    return errs
}