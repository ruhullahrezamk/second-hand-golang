package helper

import (
	"context"
	"second-hand/config"
	"strings"
	"time"

	"github.com/cloudinary/cloudinary-go"
	"github.com/cloudinary/cloudinary-go/api/uploader"
)

func ImageUpload(input interface{}) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cld, err := cloudinary.NewFromParams(config.EnvCloudName(), config.EnvCloudAPIKey(), config.EnvCloudAPISecret())
	if err != nil {
		return "", err
	}

	uploadParam, err := cld.Upload.Upload(ctx, input, uploader.UploadParams{Folder: config.EnvCloudUploadFolder()})
	if err != nil {
		return "", err
	}
	
	return uploadParam.SecureURL, nil
}

func ImageDelete(input string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cld, err := cloudinary.NewFromParams(config.EnvCloudName(), config.EnvCloudAPIKey(), config.EnvCloudAPISecret())
	if err != nil {
		return "", err
	}

	DestroyParam, err := cld.Upload.Destroy(ctx, uploader.DestroyParams {
		PublicID: input,
	  })
	if err != nil {
		return "", err
	}
	
	return DestroyParam.Response.Status, nil
}

func GetPublicId(url string) string {

	urlSplitted := strings.Split(url,"/")
	folder := urlSplitted[len(urlSplitted)-2]
	id := strings.Split(urlSplitted[len(urlSplitted)-1], ".")[0]

	return folder + "/" + id
	
}