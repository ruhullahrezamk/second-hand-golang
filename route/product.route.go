package route

import (
	"second-hand/controller"
	"second-hand/middleware"

	"github.com/gofiber/fiber/v2"
)

func ProductRouter(r *fiber.App) {

	r.Get("/product", controller.GetAllProduct)

	private := r.Group("/product", middleware.Auth)
	private.Post("/", controller.CreateProduct)
	private.Get("/myproduct", controller.GetMyProduct)
	private.Get("/:id", controller.GetOneProduct)
	private.Put("/:id", controller.UpdateProduct)
	private.Put("/publish/:id", controller.PublishProduct)
	private.Put("/status/:id", controller.StatusProduct)
	private.Delete("/:id", controller.DeleteProduct)

}