package route

import "github.com/gofiber/fiber/v2"


func NewRouter(r *fiber.App) {
	AuthRouter(r)
	ProfileRouter(r)
	DataRouter(r)
	ProductRouter(r)
	TransactionRouter(r)
	ImageRouter(r)
}

