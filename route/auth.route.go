package route

import (
	"second-hand/controller"
	"second-hand/middleware"

	"github.com/gofiber/fiber/v2"
)

func AuthRouter(r *fiber.App) {
	r.Post("/register", middleware.OptionalAuth, controller.Register)
	r.Post("/login", controller.Login)
	r.Get("/logout", controller.Logout)
}