package route

import (
	"second-hand/controller"
	"second-hand/middleware"

	"github.com/gofiber/fiber/v2"
)

func DataRouter(r *fiber.App) {

	route := r.Group("/data")
	
	route.Get("/city",controller.GetAllCity)
	route.Get("/city/:id",controller.GetCity)

	route.Get("/category",controller.GetAllCategory)
	route.Get("/category/:id",controller.GetCategory)

	route.Post("/city", middleware.Auth, middleware.IsAdmin,controller.CreateCity)
	route.Delete("/city/:id", middleware.Auth, middleware.IsAdmin, controller.DeleteCity)
	
	route.Post("/category", middleware.Auth, middleware.IsAdmin, controller.CreateCategory)
	route.Delete("/category/:id", middleware.Auth, middleware.IsAdmin,  controller.DeleteCategory)

}







