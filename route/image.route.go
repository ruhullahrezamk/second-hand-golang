package route

import (
	"second-hand/controller"
	"second-hand/middleware"

	"github.com/gofiber/fiber/v2"
)

func ImageRouter(r *fiber.App) {

	r.Post("/image", middleware.Auth, controller.AddImage)
	r.Delete("/image/:id", middleware.Auth, controller.DeleteImage)

}
