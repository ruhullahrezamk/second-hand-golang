package route

import (
	"second-hand/controller"
	"second-hand/middleware"

	"github.com/gofiber/fiber/v2"
)

func TransactionRouter(r *fiber.App) {

	api := r.Group("/transaction", middleware.Auth)

	api.Post("/", controller.CreateTransaction)
	api.Get("/offer", controller.GetAllOffer)
	api.Get("/mytransaction", controller.GetMyTransaction)
	api.Get("/:id", controller.GetTransactionById)
	api.Get("/product/:id", controller.GetTransactionByProductId)
	api.Get("/user/:id", controller.GetTransactionByUserId)
	api.Put("/:id", controller.UpdateTransactionPrice)
	api.Put("/accept/:id", controller.AcceptTransaction)
	api.Put("/sold/:id", controller.SoldStatusTransaction)
	api.Delete("/:id", controller.DeleteTransaction)

}
