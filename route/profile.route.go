package route

import (
	"second-hand/controller"
	"second-hand/middleware"

	"github.com/gofiber/fiber/v2"
)

func ProfileRouter(r *fiber.App) {

	api := r.Group("/profile")
	api.Get("/", middleware.Auth, controller.MyProfile)
	api.Get("/:id", controller.GetProfile)
	api.Put("/", middleware.Auth, controller.UpdateMyProfile)
	api.Get("/product/:id", middleware.Auth, controller.GetProductByAccount)

}