package service

import (
	"second-hand/database"
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/request"
	"second-hand/model/response"
	"second-hand/util"
)

func CreateProfile(profile *entity.Profile) {

	err := database.DB.Create(&profile).Error	
	util.PanicIfError(err)

}


func GetProfile(profile *response.Profile, id uint) bool {
	
	database.DB.Raw(util.GET_PROFILE_QUERY, id).Scan(&profile)
	
	return profile.Name != ""

}

func ValidateProfile(id uint) bool {
	var profile response.Profile 
	
	database.DB.Raw(util.GET_PROFILE_QUERY, id).Scan(&profile)
	
	return profile.City != "" && profile.PhoneNumber != ""
}

func UpdateProfile(profile *request.Profile, id uint) bool {

	var foundProfile response.Profile
	ok := GetProfile(&foundProfile,id)
	if !ok {
		return false
	}

	image_url := foundProfile.ImageUrl

	if profile.ImageUrl != "" && image_url != ""{
		publicId := helper.GetPublicId(image_url)
		_, err := helper.ImageDelete(publicId)
		util.PanicIfError(err)
	} else {
		profile.ImageUrl = image_url
	}

	var currentProfile entity.Profile

	updatedProfile := entity.Profile{
		Name : profile.Name,
		CityId: uint(profile.CityId),
		Address: profile.Address,
		PhoneNumber: profile.PhoneNumber,
		ImageUrl: profile.ImageUrl,
	}

	rowAffected := database.DB.Model(&currentProfile).Where("account_id = ?", uint(id)).Updates(updatedProfile).RowsAffected
		
	return rowAffected != 0

}