package service

import (
	"second-hand/database"
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/request"
	"second-hand/model/response"
	"second-hand/util"
)

func CreateImage(request *request.Image){
	image := entity.Image{
		ProductId: request.ProductId,
		ImageUrl: request.ImageUrl,
	}

	err := database.DB.Create(&image).Error
	util.PanicIfError(err)
}

func GetImageByProduct(id uint) []response.ProductImage{
	var image []response.ProductImage

	err := database.DB.Raw(util.GET_IMAGE_BY_PRODUCT_QUERY,id).Scan(&image).Error
	util.PanicIfError(err)

	return image
}

func CheckProductImage(id uint) (entity.Product, string, bool) {

	var product entity.Product
	var image entity.Image

	database.DB.Where("id = ?", id).Find(&image)
	if image.Id == 0 {
		return product, "",false
	}

	database.DB.Where("id = ?", image.ProductId).Find(&product)

	return product, image.ImageUrl, true
}


func DeleteImage(url string, id uint) {

	if url != ""{
		publicId := helper.GetPublicId(url)
		_, err := helper.ImageDelete(publicId)
		util.PanicIfError(err)
	} 

	var image entity.Image

	database.DB.Delete(&image, id)
	
}