package service

import (
	"second-hand/database"
	"second-hand/model/entity"
	"second-hand/model/request"
	"second-hand/model/response"
	"second-hand/util"
)

func CheckTransaction(buyer_id uint,product_id uint) bool {
	var transaction entity.Transaction

	err := database.DB.Raw(util.CHECK_TRANSACTION_QUERY,buyer_id,product_id).Scan(&transaction).Error
	util.PanicIfError(err)

	return transaction.Id != 0
}

func GetTransaction(id uint) (entity.Transaction, bool) {
	var transaction entity.Transaction

	err := database.DB.Raw(util.GET_TRANSACTION_QUERY,id).Scan(&transaction).Error
	util.PanicIfError(err)

	return transaction, transaction.Id != 0
}

func GetSellerId(product_id uint) (uint,bool) {
	var product entity.Product

	err := database.DB.Raw(util.GET_SELLER_ID_QUERY,product_id).Scan(&product).Error
	util.PanicIfError(err)

	return product.AccountId, product.Id != 0
}


func NewTransaction(request request.Transaction, buyer_id uint) (string,bool) {

	seller_id, ok := GetSellerId(request.ProductId)
	if !ok {
		return "product not found", false
	}

	if buyer_id == seller_id {
		return "you cant create transaction for your own product", false
	}

	newTransaction := entity.Transaction{
		BuyerId: buyer_id,
		Price: request.Price,
		ProductId: request.ProductId,
		SellerId: seller_id,
	}

	err := database.DB.Create(&newTransaction).Error
	util.PanicIfError(err)

	return "",true
}

func GetTransactionDetail(transaction_id uint) (response.DetailTransaction, bool) {
	var transaction response.DetailTransaction

	err := database.DB.Raw(util.GET_DETAIL_TRANSACTION_QUERY,transaction_id).Scan(&transaction).Error
	util.PanicIfError(err)

	var city entity.City

	err = database.DB.Raw(util.GET_CITY_NAME_QUERY,transaction.BuyerId).Scan(&city).Error
	util.PanicIfError(err)

	transaction.City = city.Name

	return transaction, transaction.Id != 0
}

func GetOfferByProduct(seller_id uint, role string ,product_id uint, limit int, offset int, accepted int) (response.ProductAndOffer, string) {
	
	var product response.ProductTransaction

	var productAndOffer response.ProductAndOffer

	err := database.DB.Raw(util.GET_PRODUCT_BY_ID,product_id).Scan(&product).Error
	util.PanicIfError(err)

	if product.SellerId != seller_id && role != "Admin"{
		return productAndOffer, "only seller and admin can access"
	}

	if product.ProductName == "" {
		return productAndOffer, "product not found"
	}

	var offerList []response.Offer
	err = database.DB.Raw(util.GET_OFFER_QUERY,product_id,accepted,limit,offset).Scan(&offerList).Error
	util.PanicIfError(err)

	productAndOffer.Product = product
	productAndOffer.Offer = offerList

	return productAndOffer, ""
}

func GetOfferByUserId(seller_id uint, buyer_id uint, limit int, offset int, accepted int) (response.ProfileAndOffer, string) {
	
	var profile response.ProfileBuyer

	var profileAndOffer response.ProfileAndOffer

	err := database.DB.Raw(util.GET_BUYER_PROFILE_QUERY,buyer_id).Scan(&profile).Error
	util.PanicIfError(err)

	if profile.BuyerName == "" {
		return profileAndOffer, "product not found"
	}

	var offerList []response.PriceOffer
	err = database.DB.Raw(util.GET_OFFER_BY_USER_QUERY,buyer_id,seller_id, accepted ,limit,offset).Scan(&offerList).Error
	util.PanicIfError(err)

	profileAndOffer.Profile = profile
	profileAndOffer.Offer = offerList

	if len(offerList) == 0 {
		return profileAndOffer, "transaction not found"
	}

	return profileAndOffer, ""
}

func GetAllOffer(seller_id uint, limit int, offset int, accepted int) ([]response.OfferPreview, string) {
	
	var offerList []response.OfferPreview

	err := database.DB.Raw(util.GET_ALL_OFFER,seller_id,accepted,limit,offset).Scan(&offerList).Error
	util.PanicIfError(err)

	if len(offerList) == 0 {
		return offerList, "transaction not found"
	}

	return offerList, ""
}

func GetMyTransaction(buyer_id uint, limit int, offset int, accepted int) ([]response.Transaction, string) {
	
	var transactionList []response.Transaction

	err := database.DB.Raw(util.GET_MY_TRANSACTION,buyer_id, accepted, limit, offset).Scan(&transactionList).Error
	util.PanicIfError(err)

	if len(transactionList) == 0 {
		return transactionList, "transaction not found"
	}

	return transactionList, ""
}

func UpdateTransaction(id uint, newPrice uint) bool {
	var transaction entity.Transaction

	database.DB.Where("id = ?",id).First(&transaction)
	transaction.Price = newPrice

	err := database.DB.Save(transaction).Error
	util.PanicIfError(err)

	return transaction.Price == newPrice
}

func AcceptTransaction(id uint) bool {
	var transaction entity.Transaction

	database.DB.Where("id = ?",id).First(&transaction)
	transaction.Accepted = !transaction.Accepted 

	err := database.DB.Save(transaction).Error
	util.PanicIfError(err)

	return transaction.Accepted
}

func SoldTransaction(id uint) (string,bool) {
	var transaction entity.Transaction

	database.DB.Where("id = ?",id).First(&transaction)
	if transaction.Sold {
		return "transaction sold status already true",false
	}

	transaction.Sold = true
	transaction.Accepted = true

	err := database.DB.Save(transaction).Error
	util.PanicIfError(err)

	product_id := transaction.ProductId

	var deleteTransaction entity.Transaction

	database.DB.Where("product_id = ? AND id != ?", product_id, id).Delete(&deleteTransaction)

	_ = SoldProduct(product_id)

	return "",transaction.Sold
}

func DeleteTransaction(id uint) {

	var transacion entity.Transaction

	database.DB.Delete(&transacion, id)
	
}