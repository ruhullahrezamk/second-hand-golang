package service

import (
	"second-hand/database"
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/response"
	"second-hand/util"
)

func CreateProduct(product *entity.Product) (string,bool) {
	var category entity.Category
	foundCategory := GetCategory(&category,product.CategoryId)
	if !foundCategory {
		return "category not found", false
	}

	err := database.DB.Create(&product).Error
	util.PanicIfError(err)

	return "",true
}

func GetAllProduct(limit, offset int) []response.Product {
	var product []response.Product

	err := database.DB.Raw(util.GET_PRODUCT_QUERY, limit, offset).Scan(&product).Error	
	util.PanicIfError(err)	

	return product
}

func GetProductByCategory(category uint, limit, offset int) []response.Product {
	var product []response.Product

	err := database.DB.Raw(util.GET_PRODUCT_BY_CATEGORY_QUERY, category,limit, offset).Scan(&product).Error	
	util.PanicIfError(err)	

	return product
}

func GetMyProduct(limit int, offset int, sold int, id uint) []response.MyProduct {
	var product []response.MyProduct

	err := database.DB.Raw(util.GET_MY_PRODUCT_QUERY, sold, id, limit, offset).Scan(&product).Error	
	util.PanicIfError(err)	

	return product
}

func GetProductByAccountId(limit int, offset int,id uint) []response.Product {
	var product []response.Product

	err := database.DB.Raw(util.GET_PRODUCT_BY_ACCOUNT_ID_QUERY, id, limit, offset).Scan(&product).Error	
	util.PanicIfError(err)	

	return product
}


func GetProduct(product *response.DetailProduct, id uint) bool {

	err := database.DB.Raw(util.GET_ONE_PRODUCT_QUERY,id).Scan(&product).Error
	util.PanicIfError(err)	

	return product.Id != 0
}

func CheckProduct( id uint, user_id uint, role string) (string,bool) {

	var foundProduct response.DetailProduct

	ok := GetProduct(&foundProduct,id)
	if !ok {
		return "product not found", false
	}

	if foundProduct.OwnerId != user_id && role != "Admin"{
		return "only product owner can access", false
	}

	return "", foundProduct.Id != 0
}

func UpdateProduct(product *entity.Product, saved_url string, id uint) bool {

	if product.ImageUrl != "" && saved_url != ""{
		publicId := helper.GetPublicId(saved_url)
		_, err := helper.ImageDelete(publicId)
		util.PanicIfError(err)
	} 

	rowAffected := database.DB.Model(product).Where("id = ?", uint(id)).Updates(product).RowsAffected
		
	return rowAffected != 0

}

func PublishProduct(id uint) bool {
	var product entity.Product

	database.DB.Where("id = ?",id).First(&product)
	product.Published = !product.Published 

	err := database.DB.Save(product).Error
	util.PanicIfError(err)

	return product.Published
}

func SoldProduct(id uint) (bool) {
	var product entity.Product

	database.DB.Where("id = ?",id).First(&product)

	product.Sold = !product.Sold
	product.Published = !product.Sold

	err := database.DB.Save(product).Error
	util.PanicIfError(err)

	return product.Sold
}

func DeleteProduct(id uint) {

	var product entity.Product

	if product.ImageUrl != ""{
		publicId := helper.GetPublicId(product.ImageUrl)
		_, err := helper.ImageDelete(publicId)
		util.PanicIfError(err)
	} 

	database.DB.Delete(&product, id)
	
}