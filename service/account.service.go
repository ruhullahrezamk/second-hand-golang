package service

import (
	"second-hand/database"
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/response"
	"second-hand/util"

	"golang.org/x/crypto/bcrypt"
)


func GetAccount(account *entity.Account, email string) bool{

	database.DB.Select("id","email","password","role").Where("email = ?",email).Find(&account)

	return account.Id != 0
}

func CreateAccount(account entity.Account, email string, name string) bool {

	password := []byte(account.Password)
	hahashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)	
	util.PanicIfError(err)

	account.Password = string(hahashedPassword)

	foundAccount := GetAccount(&account, email)
	if foundAccount {
		return false
	}

	err = database.DB.Create(&account).Error
	util.PanicIfError(err)

	newProfile := entity.Profile{
		AccountId: uint(account.Id),
		Name: name,
	}

	CreateProfile(&newProfile)
	
	return true
}

func Login(account *entity.Account, email string, password string) (string, bool) {

	foundAccount := GetAccount(account, email)
	if !foundAccount {
		return "",false
	}

	hashedPassword := []byte(account.Password)
    err := bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil {	
		return "",false
	}
	
	var profile response.Profile
	foundProfile := GetProfile(&profile,account.Id)
	if !foundProfile {
		return "",false
	}

	token, err := helper.SignToken(account.Id, profile.Name, account.Role)
	util.PanicIfError(err)

	return token, true

}