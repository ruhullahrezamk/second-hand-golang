package service

import (
	"second-hand/database"
	"second-hand/model/entity"
	"second-hand/util"
)

func CreateCity(city *entity.City) bool {

	foundCity := database.DB.Where("name = ?",city.Name).First(&city).RowsAffected
	if foundCity != 0 {
		return false
	}

	err := database.DB.Create(&city).Error
	util.PanicIfError(err)

	return true
}

func GetCity(city *entity.City, id uint) bool {

	err := database.DB.Select("id","name").Where("id = ?",id).Find(&city).Error
	util.PanicIfError(err)

	return city.Id != 0
}

func GetAllCity() []entity.City {

	var cities []entity.City

	database.DB.Find(&cities)

	return cities
}

func DeleteCity(city *entity.City, id uint) bool {

	rowAffected := database.DB.Delete(&city, id).RowsAffected
	
	return rowAffected != 0
}

func CreateCategory(category entity.Category) bool{

	foundCategory:= database.DB.Where("name = ?",category.Name).First(&category).RowsAffected
	if foundCategory!= 0 {
		return false
	}

	err := database.DB.Create(&category).Error
	util.PanicIfError(err)

	return true
}

func FindCategory(name string) uint {

	var category entity.Category

	err := database.DB.Select("id","name").Where("name = ?",name).Find(&category).Error
	util.PanicIfError(err)
	
	return category.Id 
}

func GetCategory(category *entity.Category, id uint) bool {

	err := database.DB.Select("id","name").Where("id = ?",id).Find(&category).Error
	util.PanicIfError(err)
	
	return category.Id != 0
	
}

func GetAllCategory() []entity.Category {

	var categories []entity.Category

	database.DB.Find(&categories)

	return categories
}

func DeleteCategory(category *entity.Category, id uint) bool {

	rowAffected := database.DB.Delete(&category, id).RowsAffected
	
	return rowAffected != 0
}