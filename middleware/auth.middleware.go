package middleware

import (
	"second-hand/helper"
	"second-hand/util"

	"github.com/gofiber/fiber/v2"
)

func Auth(c *fiber.Ctx) error {

	token := c.Cookies("Authorization")
	payload, ok := helper.ValidateToken(token)

	if !ok {
		return util.Unauthorized(c)
	}

	c.Locals("username", payload.Name)
	c.Locals("user_id", payload.UserId)
	c.Locals("role", payload.Role)

	return c.Next()

}

func OptionalAuth(c *fiber.Ctx) error {

	token := c.Cookies("Authorization")
	payload, ok := helper.ValidateToken(token)

	if !ok {
		return c.Next()
	}

	c.Locals("username", payload.Name)
	c.Locals("user_id", payload.UserId)
	c.Locals("role", payload.Role)

	return c.Next()

}

func IsAdmin(c *fiber.Ctx) error {

	user_role := c.Locals("role").(string)

	if user_role != "Admin" {
		return util.Forbidden(c, "only admin can access")
	}

	return c.Next()

}
