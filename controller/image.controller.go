package controller

import (
	"second-hand/helper"
	"second-hand/model/request"
	"second-hand/model/response"
	"second-hand/service"
	"second-hand/util"

	"github.com/gofiber/fiber/v2"
	"github.com/leebenson/conform"
)

func AddImage(c *fiber.Ctx) error {

	var request request.Image

	err := c.BodyParser(&request)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	errorList, ok := helper.Validate(request)
	if !ok {
		return util.BadRequest(c, errorList)
	}

	conform.Strings(&request)

	user_id := c.Locals("user_id").(uint)
	role := c.Locals("role").(string)

	var foundProduct response.DetailProduct
	ok = service.GetProduct(&foundProduct,request.ProductId)
	if !ok {
		return util.NotFound(c,"product not found")
	}

	validUser := foundProduct.OwnerId == user_id || role == "Admin"

	if !validUser {
		return util.BadRequest(c,"only product owner and admin can access")
	}

	formHeader, err := c.FormFile("image")
	if err == nil {
		formFile, err := formHeader.Open()
		if err != nil {
			return util.BadRequest(c, err.Error())
		}

		fileType := formHeader.Header["Content-Type"][0]
		fileSize := formHeader.Size

		message, ok := util.ValidateFile(fileType, fileSize)
		if !ok {
			return util.BadRequest(c, message)
		}

		url, err := helper.ImageUpload(formFile)
		if err != nil {
			return util.BadRequest(c, err.Error())
		}

		request.ImageUrl = url
	}

	service.CreateImage(&request)
	
	return util.Created(c, "successfuly upload image")
}


func DeleteImage(c *fiber.Ctx) error {

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	user_id := c.Locals("user_id").(uint)
	role := c.Locals("role").(string)

	foundProduct, imageUrl, ok := service.CheckProductImage(uint(id))
	if !ok {
		return util.NotFound(c,"image not found")
	}

	if foundProduct.Id == 0 {
		return util.NotFound(c,"product not found")
	}

	validUser := role == "Admin" || foundProduct.AccountId == user_id
	if !validUser {
		return util.BadRequest(c,"only product owner and admin can access")
	}

	service.DeleteImage(imageUrl,uint(id))

	return util.Created(c, "successfuly delete image")
}