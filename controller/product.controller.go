package controller

import (
	"fmt"
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/request"
	"second-hand/model/response"
	"second-hand/service"
	"second-hand/util"

	"github.com/gofiber/fiber/v2"
	"github.com/leebenson/conform"
)

func CreateProduct(c *fiber.Ctx) error {

	var product request.Product

	err := c.BodyParser(&product)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}
	
	errorList, ok := helper.Validate(product)
	if !ok {
		return util.BadRequest(c, errorList)
	}

	conform.Strings(&product)

	user_id := c.Locals("user_id").(uint)

	haveAccessToCreate := service.ValidateProfile(user_id)
	if !haveAccessToCreate {
		return util.BadRequest(c, "please complete your profile first")
	}

	formHeader, err := c.FormFile("image")
	if err == nil {
		formFile, err := formHeader.Open()
		if err != nil {
			return util.BadRequest(c,err.Error())
		}
		
		fileType := formHeader.Header["Content-Type"][0]
		fileSize := formHeader.Size
	
		message, ok := util.ValidateFile(fileType,fileSize)
		if !ok {	
			return util.BadRequest(c,message)
		}
	
		url, err := helper.ImageUpload(formFile)
		if err != nil {
			return util.BadRequest(c,err.Error())
		}
		
		product.ImageUrl = url
	}

	newProduct:= entity.Product{
		AccountId: user_id,
		Name: product.Name,
		Price: product.Price,
		CategoryId: product.CategoryId,
		Description: product.Description,
		Sold: false,
		Published: false,
		ImageUrl: product.ImageUrl,
	}

	errorMessage,ok := service.CreateProduct(&newProduct)
	if !ok {
		return util.BadRequest(c, errorMessage)
	}

	message := fmt.Sprintf("%s added to database successfuly", product.Name)
	return util.Created(c, message)
}

func GetAllProduct(c *fiber.Ctx) error {

	limit, offset := util.Pagination(c.Query("page"),c.Query("row"))
	category := c.Query("category")

	categoryId := service.FindCategory(category)
	if categoryId != 0{
		allProduct := service.GetProductByCategory(categoryId,limit,offset)
		if len(allProduct) == 0 {
			return util.NotFound(c, "product not found")
		}
	
		return util.Success(c,allProduct)
	}

	allProduct := service.GetAllProduct(limit,offset)
	if len(allProduct) == 0 {
		return util.NotFound(c, "product not found")
	}

	return util.Success(c,allProduct)

}

func GetMyProduct(c *fiber.Ctx) error {

	limit, offset := util.Pagination(c.Query("page"),c.Query("row"))
	sold := util.GetStatusParam(c.Query("sold"))

	user_id := c.Locals("user_id").(uint)
	allProduct := service.GetMyProduct(limit, offset, sold, user_id)
	if len(allProduct) == 0 {
		return util.NotFound(c, "product not found")
	}

	return util.Success(c,allProduct)

}

func GetProductByAccount(c *fiber.Ctx) error {

	limit, offset := util.Pagination(c.Query("page"),c.Query("row"))

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	allProduct := service.GetProductByAccountId(limit,offset,uint(id))
	if len(allProduct) == 0 {
		return util.NotFound(c, "product not found")
	}

	return util.Success(c,allProduct)
}

func GetOneProduct(c *fiber.Ctx) error {

	var product response.DetailProduct

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	user_id := c.Locals("user_id").(uint)
	role := c.Locals("role").(string)

	foundProduct := service.GetProduct(&product,uint(id))
	if !foundProduct {
		return util.NotFound(c,"Product not found")
	}

	validUser := user_id != product.OwnerId && role != "Admin"

	if !product.Published  && validUser {
		return util.NotFound(c,"Product not found")
	}

	if product.Sold && validUser {
		return util.NotFound(c,"Product not found")
	}

	imageList := service.GetImageByProduct(uint(id))

	dataProduct := &response.ProductAndImage{
		Product: product,
		Image: imageList,
	}
	
	return util.Success(c, dataProduct)
}

func UpdateProduct(c *fiber.Ctx) error {

	var product request.Product

	err := c.BodyParser(&product)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	errorList, ok := helper.Validate(product)
	if !ok {
		return util.BadRequest(c, errorList)
	}

	conform.Strings(&product)
	
	user_id := c.Locals("user_id").(uint)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	var foundProduct response.DetailProduct
	ok = service.GetProduct(&foundProduct,uint(id))
	if !ok {
		return util.NotFound(c,"product not found")
	}

	if foundProduct.OwnerId != user_id {
		return util.BadRequest(c,"only product owner can access")
	}

	formHeader, err := c.FormFile("image_1")
	if err == nil {
		formFile, err := formHeader.Open()
		if err != nil {
			return util.BadRequest(c,err.Error())
		}
		
		fileType := formHeader.Header["Content-Type"][0]
		fileSize := formHeader.Size
	
		message, ok := util.ValidateFile(fileType,fileSize)
		if !ok {	
			return util.BadRequest(c,message)
		}
	
		url, err := helper.ImageUpload(formFile)
		if err != nil {
			return util.BadRequest(c,err.Error())
		}
		
		product.ImageUrl = url
	}

	updatedProduct:= entity.Product{
		AccountId: user_id,
		Name: product.Name,
		Price: product.Price,
		CategoryId: product.CategoryId,
		Description: product.Description,
		Sold: false,
		Published: false,
		ImageUrl: product.ImageUrl,
	}

	ok = service.UpdateProduct(&updatedProduct, foundProduct.ImageUrl ,uint(id))
	if !ok {
		return util.NotFound(c,"product not found")
	}

	message := fmt.Sprintf("successfully update product with id : %d", id)
	return util.Created(c, message)
}

func PublishProduct(c *fiber.Ctx) error {
	
	user_id := c.Locals("user_id").(uint)
	role := c.Locals("role").(string)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	errorMessage, ok := service.CheckProduct(uint(id),user_id,role)
	if ! ok {
		return util.BadRequest(c,errorMessage)
	}

	status := service.PublishProduct(uint(id))

	if status {
		return util.Success(c,"product has been published")
	}

	return util.Success(c,"product has been unpublished")

}

func StatusProduct(c *fiber.Ctx) error {
	
	user_id := c.Locals("user_id").(uint)
	role := c.Locals("role").(string)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	errorMessage, ok := service.CheckProduct(uint(id),user_id,role)
	if ! ok {
		return util.BadRequest(c,errorMessage)
	}

	status := service.SoldProduct(uint(id))

	if status{
		return util.Success(c,"product sold")
	}

	return util.Success(c,"product still available")

}

func DeleteProduct(c *fiber.Ctx) error {
	
	user_id := c.Locals("user_id").(uint)
	role := c.Locals("role").(string)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	errorMessage, ok := service.CheckProduct(uint(id),user_id,role)
	if ! ok {
		return util.BadRequest(c,errorMessage)
	}

	service.DeleteProduct(uint(id))
	
	return util.Success(c,"product deleted")

}
