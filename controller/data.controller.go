package controller

import (
	"fmt"
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/request"
	"second-hand/model/response"
	"second-hand/service"
	"second-hand/util"

	"github.com/gofiber/fiber/v2"
	"github.com/leebenson/conform"
)

func GetCity(c *fiber.Ctx) error {

	var city entity.City

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	foundCity := service.GetCity(&city,uint(id))
	if !foundCity {
		return util.NotFound(c,"city not found")
	}
	
	cityData := response.Data{
		Id : city.Id,
		Name : city.Name,
	}

	return util.Success(c, cityData)
}

func GetAllCity(c *fiber.Ctx) error {

	cities := service.GetAllCity()
	if len(cities) == 0 {
		return util.NotFound(c,"city data still empty")
	}
	
	cityData := []response.Data{}

	for _, data := range cities{
		city := response.Data{
			Id : data.Id,
			Name : data.Name,
		}
		cityData = append(cityData, city)
	}

	return util.Success(c, cityData)
}

func CreateCity(c *fiber.Ctx) error {

	var city request.Data

	err := c.BodyParser(&city)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	errorList, ok := helper.Validate(city)
	if !ok {
		return util.BadRequest(c, errorList)
	}

	conform.Strings(&city)

	newCity := entity.City{
		Name: city.Name,
	}

	createdCity := service.CreateCity(&newCity)
	if !createdCity  {
		return util.BadRequest(c, "city name already registered")
	}

	message := fmt.Sprintf("%s added to database successfuly", city.Name)
	return util.Created(c, message)
}

func DeleteCity(c *fiber.Ctx) error {
	var city entity.City

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	deletedCity := service.DeleteCity(&city,uint(id))
	if !deletedCity {
		return util.NotFound(c,"city not found")
	}
	
	return util.Success(c, "succesfuly delete city")
}

func GetCategory(c *fiber.Ctx) error {

	var category entity.Category

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	foundCategory := service.GetCategory(&category,uint(id))
	if !foundCategory {
		return util.NotFound(c,"Category not found")
	}
	
	categoryData := response.Data{
		Id : category.Id,
		Name : category.Name,
	}

	return util.Success(c, categoryData)
}

func GetAllCategory(c *fiber.Ctx) error {

	categories := service.GetAllCategory()
	if len(categories) == 0 {
		return util.NotFound(c,"Category data still empty")
	}
	
	categoryData := []response.Data{}

	for _, data := range categories{
		category := response.Data{
			Id : data.Id,
			Name : data.Name,
		}
		categoryData = append(categoryData, category)
	}

	return util.Success(c, categoryData)
}

func CreateCategory(c *fiber.Ctx) error {

	var category request.Data

	err := c.BodyParser(&category)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}
	errorList, ok := helper.Validate(category)
	if !ok {
		return util.BadRequest(c, errorList)
	}

	conform.Strings(&category)

	newCategory := entity.Category{
		Name: category.Name,
	}

	createdCategory := service.CreateCategory(newCategory)
	if !createdCategory  {
		return util.BadRequest(c, "Category name already registered")
	}

	message := fmt.Sprintf("%s added to database successfuly", category.Name)
	return util.Created(c, message)
}

func DeleteCategory(c *fiber.Ctx) error {
	var data entity.Category

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	deletedData := service.DeleteCategory(&data,uint(id))
	if !deletedData {
		return util.NotFound(c,"Data not found")
	}
	
	return util.Success(c, "succesfuly delete Category")
}


