package controller

import (
	"fmt"
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/request"
	"second-hand/service"
	"second-hand/util"
	"github.com/leebenson/conform"
	"github.com/gofiber/fiber/v2"
)

func Register(c *fiber.Ctx) error {
	
	var user request.Register

	err := c.BodyParser(&user)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	conform.Strings(&user) 

	errorList,ok := helper.Validate(user)
	if !ok {
		return util.BadRequest(c,errorList)
	}

	registerRole := "User"

	currentRole := c.Locals("role")
	if currentRole == "Admin" {
		registerRole = "Admin"
	}

	newAccount := entity.Account{
		Email: user.Email,
		Password: user.Password,
		Role: registerRole,
	}

	createdAccount := service.CreateAccount(newAccount,user.Email,user.Name)
	if !createdAccount {
		return util.BadRequest(c, "email already used")
	}

	message := fmt.Sprintf("New account with email %s has been registered", user.Email)
	return util.Created(c, message)

}

func Login(c *fiber.Ctx) error {
	
	var user request.Login

	err := c.BodyParser(&user)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	conform.Strings(&user) 
	
	logginAccount := entity.Account{
		Email: user.Email,
	}

	token, ok := service.Login(&logginAccount,user.Email,user.Password)
	if !ok {
		return util.BadRequest(c, "incorrect email or password")
	}

	c.Cookie(&fiber.Cookie{
		Name : "Authorization",
		HTTPOnly : true,
		Value : token,
	})

	return util.Success(c, "login success")
}

func Logout(c *fiber.Ctx) error {
	
	c.ClearCookie("Authorization")

	return util.Success(c, "logout success")
}