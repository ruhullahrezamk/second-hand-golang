package controller

import (
	"second-hand/helper"
	"second-hand/model/request"
	"second-hand/service"
	"second-hand/util"

	"github.com/gofiber/fiber/v2"
	"github.com/leebenson/conform"
)

func CreateTransaction(c *fiber.Ctx) error {

	var transaction request.Transaction

	err := c.BodyParser(&transaction)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	conform.Strings(&transaction)

	errorList, ok := helper.Validate(transaction)
	if !ok {
		return util.BadRequest(c, errorList)
	}

	user_id := c.Locals("user_id").(uint)

	haveAccessToCreate := service.ValidateProfile(user_id)
	if !haveAccessToCreate {
		return util.BadRequest(c, "please complete your profile first")
	}

	foundTransaction := service.CheckTransaction(user_id,transaction.ProductId)
	if foundTransaction{
		return util.BadRequest(c, "you already create transaction for this product")
	}

	errorMessage,ok := service.NewTransaction(transaction,user_id)
	if !ok {
		return util.BadRequest(c, errorMessage)
	}

	return util.Created(c,"successfully create transaction")

}

func GetTransactionById(c *fiber.Ctx) error {
	
	user_id := c.Locals("user_id").(uint)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err)

	role := c.Locals("role").(string)

	foundTransaction, ok := service.GetTransaction(uint(id))
	if !ok {
		return util.NotFound(c, "transaction not found")
	}

	validUser := foundTransaction.BuyerId == user_id || foundTransaction.SellerId == user_id || role == "Admin"
	if !validUser {	
		return util.Forbidden(c, "only valid user can access")
	}

	transactionData, ok := service.GetTransactionDetail(uint(id))
	if !ok {
		return util.NotFound(c, "transaction not found")
	}

	return util.Success(c,transactionData)
}

func GetTransactionByProductId(c *fiber.Ctx) error {

	user_id := c.Locals("user_id").(uint)
	limit, offset := util.Pagination(c.Query("page"),c.Query("row"))
	accepted := util.GetStatusParam(c.Query("accepted"))

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err)

	role := c.Locals("role").(string)

	offerList, errorMessage := service.GetOfferByProduct(user_id,role,uint(id),limit,offset,accepted)
	if errorMessage != "" {
		return util.BadRequest(c, errorMessage)
	}
	
	return util.Success(c, offerList)
}

func GetTransactionByUserId(c *fiber.Ctx) error {

	user_id := c.Locals("user_id").(uint)
	limit, offset := util.Pagination(c.Query("page"),c.Query("row"))
	accepted := util.GetStatusParam(c.Query("accepted"))

	buyer_id ,err := c.ParamsInt("id")
	util.PanicIfError(err)

	offerList, errorMessage := service.GetOfferByUserId(user_id, uint(buyer_id), limit, offset, accepted)
	if errorMessage != "" {
		return util.NotFound(c, errorMessage)
	}
	
	return util.Success(c, offerList)

}

func GetAllOffer(c *fiber.Ctx) error {

	user_id := c.Locals("user_id").(uint)
	limit, offset := util.Pagination(c.Query("page"),c.Query("row"))
	accepted := util.GetStatusParam(c.Query("accepted"))

	offerList, errorMessage := service.GetAllOffer(user_id,limit,offset,accepted)
	if errorMessage != "" {
		return util.NotFound(c, errorMessage)
	}
	
	return util.Success(c, offerList)

}


func GetMyTransaction(c *fiber.Ctx) error {

	user_id := c.Locals("user_id").(uint)
	limit, offset := util.Pagination(c.Query("page"),c.Query("row"))
	accepted := util.GetStatusParam(c.Query("accepted"))

	offerList, errorMessage := service.GetMyTransaction(user_id,limit,offset,accepted)
	if errorMessage != "" {
		return util.NotFound(c, errorMessage)
	}
	
	return util.Success(c, offerList)

}

func UpdateTransactionPrice(c *fiber.Ctx) error {

	var transaction request.UpdateTransaction

	err := c.BodyParser(&transaction)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	conform.Strings(&transaction)

	errorList, ok := helper.Validate(transaction)
	if !ok {
		return util.BadRequest(c, errorList)
	}

	user_id := c.Locals("user_id").(uint)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err)

	role := c.Locals("role").(string)

	foundTransaction, ok := service.GetTransaction(uint(id))
	if !ok {
		return util.NotFound(c, "transaction not found")
	}

	validUser := foundTransaction.BuyerId == user_id || role == "Admin"
	if !validUser {	
		return util.Forbidden(c, "only valid user can access")
	}

	if foundTransaction.Accepted {	
		return util.BadRequest(c, "transaction already accepted")
	}

	ok = service.UpdateTransaction(uint(id), transaction.Price)
	if !ok {
		return util.NotFound(c, "transaction not found")
	}

	return util.Success(c,"transaction price offer updated")
}

func AcceptTransaction(c *fiber.Ctx) error {

	user_id := c.Locals("user_id").(uint)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err)

	role := c.Locals("role").(string)

	foundTransaction, ok := service.GetTransaction(uint(id))
	if !ok {
		return util.NotFound(c, "transaction not found")
	}

	validUser := foundTransaction.SellerId == user_id || role == "Admin"
	if !validUser {
		return util.Forbidden(c, "only seller can access")
	}

	status := service.AcceptTransaction(uint(id))
	
	if status{
		return util.Success(c,"transaction accepted status : true")
	}

	return util.Success(c,"transaction accepted status : false")

}


func SoldStatusTransaction(c *fiber.Ctx) error {

	user_id := c.Locals("user_id").(uint)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err)

	role := c.Locals("role").(string)

	foundTransaction, ok := service.GetTransaction(uint(id))
	if !ok {
		return util.NotFound(c, "transaction not found")
	}

	validUser := foundTransaction.SellerId == user_id || role == "Admin"
	if !validUser {
		return util.Forbidden(c, "only seller can access")
	}

	errorMessage,status := service.SoldTransaction(uint(id))

	if !status {
		return util.Success(c,errorMessage)
	}
	
	return util.Success(c,"transaction sold status : true")
	
}

func DeleteTransaction(c *fiber.Ctx) error {
	
	user_id := c.Locals("user_id").(uint)

	id ,err := c.ParamsInt("id")
	util.PanicIfError(err)

	role := c.Locals("role").(string)

	foundTransaction, ok := service.GetTransaction(uint(id))
	if !ok {
		return util.NotFound(c, "transaction not found")
	}

	validUser := foundTransaction.SellerId == user_id || role == "Admin"
	if !validUser {
		return util.Forbidden(c, "only seller can access")
	}

	service.DeleteTransaction(uint(id))
	
	return util.Success(c,"transaction deleted")

}

