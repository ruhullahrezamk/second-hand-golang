package controller

import (
	"second-hand/helper"
	"second-hand/model/entity"
	"second-hand/model/request"
	"second-hand/model/response"
	"second-hand/service"
	"second-hand/util"

	"github.com/gofiber/fiber/v2"
	"github.com/leebenson/conform"
)

func MyProfile(c *fiber.Ctx) error {

	var profile response.Profile
	
	user_id := c.Locals("user_id").(uint)

	foundProfile := service.GetProfile(&profile, user_id)
	if !foundProfile {
		return util.NotFound(c, "profile not found")
	}

	return util.Success(c, profile)

}

func GetProfile(c *fiber.Ctx) error {

	var profile response.Profile
	
	id ,err := c.ParamsInt("id")
	util.PanicIfError(err )

	foundProfile := service.GetProfile(&profile, uint(id))
	if !foundProfile {
		return util.NotFound(c, "profile not found")
	}

	return util.Success(c, profile)

}

func UpdateMyProfile(c *fiber.Ctx) error {

	var profile request.Profile

	err := c.BodyParser(&profile)
	if err != nil {
		return util.BadRequest(c, err.Error())
	}

	conform.Strings(&profile)
	
	errorList,ok := helper.Validate(profile)
	if !ok {
		return util.BadRequest(c,errorList)
	}

	formHeader, err := c.FormFile("file")
	if err == nil {
		formFile, err := formHeader.Open()
		if err != nil {
			return util.BadRequest(c,err.Error())
		}
		
		fileType := formHeader.Header["Content-Type"][0]
		fileSize := formHeader.Size
	
		message, ok := util.ValidateFile(fileType,fileSize)
		if !ok {	
			return util.BadRequest(c,message)
		}
	
		url, err := helper.ImageUpload(formFile)
		if err != nil {
			return util.BadRequest(c,err.Error())
		}
		
		profile.ImageUrl = url
	}

	user_id := c.Locals("user_id").(uint)
	
	var city entity.City
	foundCity := service.GetCity(&city,uint(profile.CityId))
	if !foundCity {
		return util.NotFound(c, "city data not found")
	}

	success := service.UpdateProfile(&profile, uint(user_id))
	if !success {
		return util.NotFound(c, "profile not found")
	}

	return util.Success(c, "profile updated successfully")
	
}