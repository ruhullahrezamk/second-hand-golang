package entity

import (
	"time"
)

type Image struct {
	Id        		uint   	`json:"id" gorm:"primaryKey"`
	ProductId    	uint 	`json:"product_id" gorm:"type:integer"`
	ImageUrl   		string 	`json:"image_url" gorm:"type:varchar(255)"`
	CreatedAt 		time.Time `json:"created_at"`
}