package entity

import (
	"time"
	"gorm.io/gorm"
)

type Account struct {
	Id        	uint   	`json:"id" gorm:"primaryKey"`
	Email     	string 	`json:"email" gorm:"type:varchar(255)"`
	Password     string `json:"password" gorm:"type:varchar(255)"`
	Role    	string 	`json:"role" gorm:"type:varchar(10)"`
	Profile 	Profile `gorm:"Foreignkey:AccountId;association_foreignkey:Id"`
	CreatedAt 	time.Time `json:"created_at"`
	UpdatedAt 	time.Time `json:"updated_at"`
	IsDeleted 	gorm.DeletedAt `json:"is_deleted" gorm:"index"` 
}