package entity

import (
	"time"
	"gorm.io/gorm"
)

type City struct {
	Id        uint   `json:"id" gorm:"primaryKey"`
	Name     string `json:"name" gorm:"type:varchar(255)"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	IsDeleted gorm.DeletedAt `json:"is_deleted" gorm:"index"` 
}