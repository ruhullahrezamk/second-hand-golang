package entity

import (
	"time"
	"gorm.io/gorm"
)

type Profile struct {
	Id        		uint   	`json:"id" gorm:"primaryKey"`
	AccountId    	uint 	`json:"account_id" gorm:"type:integer"`
	Name     		string 	`json:"name" gorm:"type:varchar(255)"`
	CityId   		uint 	`json:"city_id" gorm:"type:integer"`
	Address   		string 	`json:"address" gorm:"type:varchar(350)"`
	PhoneNumber   	string 	`json:"phone_number" gorm:"type:varchar(17)"`
	ImageUrl   		string 	`json:"image_url" gorm:"type:varchar(255)"`
	CreatedAt 		time.Time `json:"created_at"`
	UpdatedAt 		time.Time `json:"updated_at"`
	IsDeleted 		gorm.DeletedAt `json:"is_deleted" gorm:"index"` 
}