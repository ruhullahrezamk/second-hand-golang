package entity

import (
	"time"
	"gorm.io/gorm"
)

type Product struct {
	Id        	uint  	`json:"id" gorm:"primaryKey"`
	AccountId	uint 	`json:"account_id"`
	Name     	string 	`json:"name" gorm:"type:varchar(255)"`
	Price     	uint 	`json:"price"`
	CategoryId	uint 	`json:"category_id"`
	Description string  `json:"description"`
	ImageUrl   		string 	`json:"image_url" gorm:"type:varchar(255)"`
	Sold 		bool  	`json:"is_sold" gorm:"type:boolean"`
	Published 	bool  	`json:"published" gorm:"type:boolean"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	IsDeleted gorm.DeletedAt `json:"is_deleted" gorm:"index"` 
}