package entity

import (
	"time"
	"gorm.io/gorm"
)

type Transaction struct {
	Id        	uint  	`json:"id" gorm:"primaryKey"`
	ProductId	uint 	`json:"product_id"`
	SellerId	uint 	`json:"seller_id"`
	BuyerId		uint 	`json:"buyer_id"`
	Price     	uint 	`json:"price"`
	Sold 		bool  	`json:"is_sold" gorm:"type:boolean"`
	Accepted	bool  	`json:"accepted" gorm:"type:boolean"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	IsDeleted gorm.DeletedAt `json:"is_deleted" gorm:"index"` 
}