package request

import (
	_ "github.com/go-playground/validator/v10"
)

type Login struct {
	Email     string `json:"email" conform:"lower" validate:"required,email"`
	Password     string `json:"password" validate:"required"`
}