package request

import (
	_ "github.com/go-playground/validator/v10"
)

type Image struct {
	ProductId	uint 	`form:"product_id" gorm:"type:unsigned" conform:"number" validate:"required,gte=0"`
	ImageUrl	string 
}