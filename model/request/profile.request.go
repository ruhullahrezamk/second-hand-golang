package request

import (
	_ "github.com/go-playground/validator/v10"
)

type Profile struct {
	Name		string `form:"name" conform:"name" validate:"required"`
	CityId		int`form:"city_id" validate:"required,gte=0"`
	Address		string `form:"address" conform:"!html,trim" validate:"required"`
	PhoneNumber string `form:"phone_number" validate:"required,e164"`
	ImageUrl	string 
}