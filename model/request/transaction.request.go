package request

import (
	_ "github.com/go-playground/validator/v10"
)

type Transaction struct {
	ProductId   uint 	`json:"product_id" conform:"number" validate:"required,gte=0"`
	Price     	uint 	`json:"price" gorm:"type:unsigned" conform:"number" validate:"required,gte=500"`
}

type UpdateTransaction struct {
	Price     	uint 	`json:"price" gorm:"type:unsigned" conform:"number" validate:"required,gte=500"`
}