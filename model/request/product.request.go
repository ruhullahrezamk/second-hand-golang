package request

import (
	_ "github.com/go-playground/validator/v10"
)

type Product struct {
	Name     	string 	`form:"name" conform:"!html,trim" validate:"required,lte=255"`
	Price     	uint 	`form:"price" gorm:"type:unsigned" conform:"number" validate:"required,gte=500"`
	CategoryId	uint 	`form:"category_id" gorm:"type:unsigned" conform:"number" validate:"required,gte=0"`
	Description string  `form:"description" gorm:"type:text" conform:"!html,trim" validate:"required"`
	ImageUrl	string 
}