package request

import (
	_ "github.com/go-playground/validator/v10"
)

type Data struct {
	Name		string `json:"name" conform:"name" validate:"required"`
}