package request

import (
	_ "github.com/go-playground/validator/v10"
)

type Register struct {
	Name     string `json:"name" conform:"name" validate:"required"`
	Email     string `json:"email" conform:"lower" validate:"required,email"`
	Password     string `json:"password" validate:"required,gte=5,lte=255"`
}
