package response

import "time"

type Offer struct{
	Id            	uint      `json:"transaction_id"`
	BuyerName     	string    `json:"buyer_name"`
	BuyerId    		string    `json:"buyer_id"`
	PhoneNumber   	string    `json:"phone_number"`
	City          	string    `json:"city"`
	Accepted      	bool      `json:"accepted"`
	ProfileImageUrl string    `json:"profile_image_url"`	
	PriceOffered 	uint	  `json:"price_offered"`
	CreatedAt     	time.Time `json:"created_at"`
}

type ProductTransaction struct {
	ProductImageUrl string    `json:"product_image_url"`
	ProductId   	uint   	  `json:"product_id"`
	ProductName   	string    `json:"product_name"`
	Price         	uint      `json:"price"`
	SellerId       	uint      `json:"seller_id"`
}

type ProductAndOffer struct {
	Product ProductTransaction
	Offer []Offer
}
