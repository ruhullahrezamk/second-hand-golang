package response

type Product struct {
	Id		uint `json:"id"`
	Name		string `json:"name"`
	Price		uint `json:"price"`
	Category	string `json:"category"`
	ImageUrl	string `json:"image_url"`
}

type MyProduct struct {
	Id			uint `json:"id"`
	Name		string `json:"name"`
	Price		uint `json:"price"`
	Category	string `json:"category"`
	ImageUrl	string `json:"image_url"`
	Published	bool `json:"published"`
	Sold		bool `json:"sold"`
}