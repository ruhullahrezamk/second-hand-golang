package response

type Profile struct {
	Email 		string `json:"email"`
	Name		string `json:"name"`
	Address		string `json:"address"`
	City		string `json:"city"`
	PhoneNumber string `json:"phone_number"`
	ImageUrl	string `json:"imageUrl"`
}