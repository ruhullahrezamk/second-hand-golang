package response

import "time"

type DetailTransaction struct {
	Id            uint      `json:"id"`
	BuyerName     string    `json:"buyer_name"`
	BuyerId    string    `json:"buyer_id"`
	PhoneNumber   string    `json:"phone_number"`
	City          string    `json:"city"`
	ProfileImageUrl string    `json:"profile_image_url"`
	ProductImageUrl string    `json:"product_image_url"`
	Accepted      bool      `json:"accepted"`
	ProductName   string    `json:"product_name"`
	Price         uint      `json:"price"`
	PriceOffered  uint	    `json:"price_offered"`
	CreatedAt     time.Time `json:"created_at"`
}