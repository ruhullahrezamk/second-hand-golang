package response

type Data struct {
	Id	 uint `json:"id"`
	Name string `json:"name"`
}