package response

import "time"

type ProfileBuyer struct{
	BuyerName     	string    `json:"buyer_name"`
	PhoneNumber   	string    `json:"phone_number"`
	City          	string    `json:"city"`
	ProfileImageUrl string    `json:"profile_image_url"`	
}

type PriceOffer struct {
	ProductImageUrl string    `json:"product_image_url"`
	ProductName   	string    `json:"product_name"`
	Price         	uint      `json:"price"`
	PriceOffered 	uint	  `json:"price_offered"`
	TransactionId	uint	  `json:"transaction_id"`
	Accepted		bool 	  `json:"accepted"`
	Sold			bool 	  `json:"sold"`
	CreatedAt     	time.Time `json:"created_at"`
}

type ProfileAndOffer struct {
	Profile ProfileBuyer
	Offer []PriceOffer
}