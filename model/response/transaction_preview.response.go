package response

import "time"

type OfferPreview struct{
	TransactionId	uint	  `json:"transaction_id"`
	BuyerName     	string    `json:"buyer_name"`
	ProfileImageUrl string    `json:"profile_image_url"`
	ProductImageUrl string    `json:"product_image_url"`
	ProductName   	string    `json:"product_name"`
	ProductId  		uint   	  `json:"product_id"`
	Price         	uint      `json:"price"`
	PriceOffered 	uint	  `json:"price_offered"`
	Accepted		bool 	  `json:"accepted"`
	Sold			bool 	  `json:"sold"`
	CreatedAt     	time.Time `json:"created_at"`	
}