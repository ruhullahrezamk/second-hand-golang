package response

type DetailProduct struct {
	Id			uint `json:"id"`
	Name		string `json:"name"`
	Price		uint`json:"price"`
	Description	string `json:"description"`
	Category	string `json:"category"`
	ImageUrl	string `json:"image_url"`
	Owner		string `json:"owner"`
	Sold		bool `json:"sold"`
	Published	bool `json:"published"`
	OwnerId		uint `json:"owner_id"`
	OwnerImageUrl string `json:"owner_image_url"`
	City 		string `json:"city"`
}

type ProductImage struct {
	Id	uint `json:"id"`
	Url	string `json:"url"`
}

type ProductAndImage struct {
	Product DetailProduct
	Image []ProductImage
}

