package main

import (
	"second-hand/database"
	"second-hand/route"
	"second-hand/util"
	"github.com/gofiber/fiber/v2"
    "github.com/gofiber/fiber/v2/middleware/recover"
)

func main() {
	database.Connect()

	app := fiber.New(fiber.Config{
		ErrorHandler: util.ErrorHandler,
	})
	
    app.Use(recover.New())
	
	route.NewRouter(app)

	app.Listen(":3000")
}