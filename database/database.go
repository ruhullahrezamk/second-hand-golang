package database

import (
	"fmt"
	"second-hand/config"
	"second-hand/model/entity"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func Connect() {
	
	dsn := fmt.Sprintf("%s:%s@tcp(localhost:3306)/%s?parseTime=true",config.DatabaseUsername(),config.DatabasePassword(),config.DatabaseName())

	database, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("connection to database failed")
	}

	database.AutoMigrate(
		&entity.Account{},
		&entity.City{},
		&entity.Profile{},
		&entity.Category{},
		&entity.Product{},
		&entity.Transaction{},
		&entity.Image{},
	)
	
	DB = database
	
}