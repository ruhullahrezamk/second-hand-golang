package util

import "github.com/gofiber/fiber/v2"

func formatResponse(c *fiber.Ctx, status int, message string,data interface{}) error {
	return c.Status(status).JSON(fiber.Map{
		"message": message,
		"data":    data,
	})
}

func BadRequest(c *fiber.Ctx, data interface{}) error {
	return formatResponse(c, 400, "bad request", data)
}

func NotFound(c *fiber.Ctx, data interface{}) error {
	return formatResponse(c, 404, "not found",data)
}

func Created(c *fiber.Ctx, data interface{}) error {
	return formatResponse(c, 201, "created", data)
}

func Success(c *fiber.Ctx, data interface{}) error {
	return formatResponse(c, 200, "success", data)
}

func Unauthorized(c *fiber.Ctx) error {
	return formatResponse(c, 401, "unauthorized", "invalid token")
}

func Forbidden(c *fiber.Ctx, message string) error {
	return formatResponse(c, 403, "forbidden", message)
}