package util

import "github.com/gofiber/fiber/v2"

func ErrorHandler (ctx *fiber.Ctx, err error) error {
			
	code := fiber.StatusInternalServerError

	if e, ok := err.(*fiber.Error); ok {
		code = e.Code
	}

	err = ctx.Status(code).JSON(fiber.Map{
		"message":"bad request",
		"data": err.Error(),
	})

	if err != nil {
		return ctx.Status(500).JSON(fiber.Map{
			"message":"internal server error",
			"data": "try again later",
		})
	}
	
	return nil
}