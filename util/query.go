package util

const GET_PROFILE_QUERY = `
	SELECT accounts.email, profiles.name, profiles.address, cities.name as city, profiles.phone_number, profiles.image_url
	FROM profiles 
	LEFT JOIN accounts ON accounts.id=profiles.account_id 
	LEFT JOIN cities ON profiles.city_id=cities.id 
	WHERE account_id = ?
`

const GET_PRODUCT_QUERY = `
	SELECT products.id, products.name, products.price, products.image_url, categories.name as category 
	FROM products 
	LEFT JOIN categories ON products.category_id = categories.id
	WHERE products.is_deleted IS NULL AND products.published=1 AND products.sold=0
	ORDER BY products.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_PRODUCT_BY_CATEGORY_QUERY = `
	SELECT products.id, products.name, products.price, products.image_url, categories.name as category 
	FROM products 
	LEFT JOIN categories ON products.category_id = categories.id
	WHERE products.is_deleted IS NULL AND products.published=1 AND products.sold=0 AND products.category_id = ?
	ORDER BY products.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_MY_PRODUCT_QUERY = `
	SELECT products.id, products.name, products.price, products.image_url,
	products.published, products.sold, categories.name as category 
	FROM products LEFT JOIN categories ON products.category_id = categories.id 
	WHERE products.sold = ? AND products.is_deleted IS NULL AND products.account_id = ?
	ORDER BY products.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_PRODUCT_BY_ACCOUNT_ID_QUERY = `
	SELECT products.id, products.name, products.price, products.image_url, categories.name as category 
	FROM products LEFT JOIN categories ON products.category_id = categories.id 
	WHERE products.is_deleted IS NULL AND products.account_id = ?
	ORDER BY products.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_ONE_PRODUCT_QUERY = `
	SELECT products.id, products.name, products.price, products.description, products.image_url, products.sold, products.published, 
	categories.name as category, profiles.name as owner, 
	profiles.account_id as owner_id, profiles.image_url as owner_image_url, 
	cities.name as city 
	FROM products 
	LEFT JOIN categories ON products.category_id = categories.id 
	LEFT JOIN profiles ON products.account_id = profiles.account_id
	LEFT JOIN cities ON profiles.city_id = cities.id 
	WHERE products.is_deleted IS NULL AND products.id = ?
`

const GET_SELLER_ID_QUERY = "select id,account_id FROM products where id = ? AND sold = 0 AND published = 1 AND is_deleted IS NULL"

const CHECK_TRANSACTION_QUERY = `SELECT id FROM transactions WHERE buyer_id = ? AND product_id = ?`

const GET_TRANSACTION_QUERY = `SELECT id, buyer_id, seller_id, accepted FROM transactions WHERE id = ? AND is_deleted IS NULL`

const GET_DETAIL_TRANSACTION_QUERY = `
	SELECT transactions.id, transactions.price as price_offered, transactions.accepted, transactions.created_at,
	profiles.name AS buyer_name, profiles.id AS buyer_id, profiles.phone_number, profiles.image_url AS profile_image_url,
	products.name AS product_name, products.price, products.image_url AS product_image_url
	FROM transactions
	LEFT JOIN profiles ON transactions.buyer_id = profiles.account_id
	LEFT JOIN products ON products.id = transactions.product_id
	WHERE transactions.id = ? AND transactions.is_deleted IS NULL
`

const GET_CITY_NAME_QUERY = `
	SELECT cities.id, cities.name
	FROM cities
	LEFT JOIN profiles ON profiles.city_id = cities.id
	WHERE profiles.account_id = ?
`

const GET_OFFER_QUERY = `
	SELECT 
	transactions.id, transactions.price as price_offered, transactions.created_at, transactions.accepted,
	profiles.name as buyer_name,profiles.account_id as buyer_id, profiles.phone_number,profiles.image_url as profile_image_url, 
	cities.name as city
	FROM profiles
	LEFT JOIN transactions ON transactions.buyer_id = profiles.account_id
	LEFT JOIN cities ON cities.id = profiles.city_id
	WHERE transactions.product_id = ? AND transactions.sold = 0 AND transactions.is_deleted IS NULL AND transactions.accepted = ?
	ORDER BY transactions.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_PRODUCT_BY_ID = `
	SELECT
	products.account_id as seller_id, products.name as product_name, products.image_url as product_image_url, 
	products.id as product_id, products.price
	FROM products
	WHERE id = ? AND sold = 0 and is_deleted IS NULL
`

const GET_BUYER_PROFILE_QUERY = `
	SELECT profiles.name as buyer_name, profiles.phone_number,profiles.image_url as profile_image_url, cities.name as city
	FROM profiles
	LEFT JOIN cities on cities.id = profiles.city_id
	WHERE profiles.account_id = ?
`

const GET_OFFER_BY_USER_QUERY = `
	SELECT 
	products.id as product_id, products.name as product_name, products.image_url as product_image_url, products.price, transactions.price as price_offered,
	transactions.accepted, transactions.sold, transactions.id as transaction_id
	FROM transactions
	LEFT JOIN products ON transactions.product_id = products.id
	WHERE transactions.buyer_id=? AND transactions.seller_id=? AND transactions.is_deleted IS NULL AND transactions.accepted = ?
	ORDER BY transactions.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_ALL_OFFER = `
	SELECT 
	profiles.name as buyer_name, profiles.image_url as profile_image_url, 
	products.id as product_id, products.name as product_name, products.image_url as product_image_url, products.price, 
	transactions.price as price_offered, transactions.accepted, transactions.sold, transactions.created_at, 
	transactions.id as transaction_id
	FROM transactions
	LEFT JOIN products ON products.id = transactions.product_id
	LEFT JOIN profiles ON transactions.buyer_id = profiles.id
	WHERE transactions.seller_id = ? AND transactions.is_deleted IS NULL AND transactions.accepted = ?
	ORDER BY transactions.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_MY_TRANSACTION = `
	SELECT 
	profiles.name as seller_name, profiles.image_url as profile_image_url, 
	products.id as product_id, products.name as product_name, products.image_url as product_image_url, products.price, 
	transactions.price as price_offered, transactions.accepted, transactions.sold, transactions.created_at, 
	transactions.id as transaction_id
	FROM transactions
	LEFT JOIN products ON products.id = transactions.product_id
	LEFT JOIN profiles ON transactions.seller_id = profiles.id
	WHERE transactions.buyer_id = ? AND transactions.is_deleted IS NULL AND transactions.accepted = ?
	ORDER BY products.created_at DESC
	LIMIT ? OFFSET ?
`

const GET_IMAGE_BY_PRODUCT_QUERY = `SELECT id, image_url as url FROM images WHERE product_id = ?`



