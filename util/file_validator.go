package util


func validateFileType(str string) bool {
	validType := []string{"image/png","image/webp","image/jpeg"}

	for _, v := range validType {
		if v == str {
			return true
		}
	}

	return false
}

func validateSize(size int64) bool {
	return size <= 2097152 
}

func ValidateFile(str string, size int64) (string, bool) {
	
	isValidType := validateFileType(str)
	if !isValidType {
		return `invalid file type, valid type are [image/png, image/webp, image/jpeg]`, false
	}

	isValidSize := validateSize(size)
	if !isValidSize{
		return `maximum file size 2 MB`, false
	}

	return "", true

}