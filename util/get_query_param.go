package util

import (
	"strconv"
)

func Pagination(page string, row string) (int, int) {
	limit, err := strconv.Atoi(row)
	if err != nil {
		limit = 0
	}

	offset, err := strconv.Atoi(page) 
	if err != nil {
		offset = 0
	}

	if limit == 0 {
		limit = 50
	}

	offset -= 1
	if offset < 0 {
		offset = 0
	}
	offset = limit * offset

	return limit, offset
}

func GetStatusParam(status string) int {

	if status == "true" {
		return 1
	}

	return 0
}